from __future__ import unicode_literals

from django.apps import AppConfig


class VolunteerPositionsConfig(AppConfig):
    name = 'volunteer_positions'
