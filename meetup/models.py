from __future__ import unicode_literals

from django.db import models


class Meetup(models.Model):
    location = models.CharField(max_length=200)
    meetup_date = models.DateField()
    contact_name = models.CharField(max_length=200)
    contact_phone = models.CharField(max_length=20)
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.location
