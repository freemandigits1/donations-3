from django.shortcuts import render
from. forms import ContactForm

# Create your views here.
def home(request):
    context = {

    }
    return render(request, "charities/home.html", context)
    
def contact(request):
    contactForm = ContactForm(request.POST or None)
    if contactForm.is_valid():
        pass
    return render(request, 'charities/contact.html',
        {"contactForm": contactForm})
