from django.conf.urls import url


from volunteers import views


urlpatterns = [
    url(r'$', views.volunteer, name="volunteer"),
]
